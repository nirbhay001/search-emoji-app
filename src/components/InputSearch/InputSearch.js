import React, { Component } from "react";
import "./InputSearch.css";

class InputSearch extends Component {
  handleText = (event) => {
    this.props.handleChange(event);
  };
  render() {
    return (
      <input
        type="text"
        onChange={this.handleText}
        className="Input-search"
        placeholder="Search the emoji"
      />
    );
  }
}
export default InputSearch;
