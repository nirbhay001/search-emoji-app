import React, { Component } from "react";
import "./Header.css";
import laughMeme from "../../assets/images/laughMeme.png";
import thinkMeme from "../../assets/images/thinkMeme.png";

class Header extends Component {
  render() {
    return (
      <div className="header">
        <img src={laughMeme} alt="laugh meme" />
        <div>Emoji Search</div>
        <img src={thinkMeme} alt="think meme" />
      </div>
    );
  }
}

export default Header;
