import React, { Component } from "react";
import Header from "./Header/Header";
import InputSearch from "./InputSearch/InputSearch";
import Emojis from "./Emojis";
import emojiList from "../assets/EmojiData/Emoji-List.json";

class Main extends Component {
  state = {
    emojiData: emojiList,
    emojiSetData: emojiList,
  };
  handleChange = (event) => {
    let searchText = event.target.value;
    this.setState({
      emojiData: this.state.emojiSetData.filter((emoji) => {
        if (emoji.title.toLowerCase().includes(searchText.toLowerCase())) {
          return true;
        }
        return false;
      }),
    });
  };
  render() {
    return (
      <>
        <Header />
        <InputSearch handleChange={this.handleChange} />
        <Emojis emojiData={this.state.emojiData} />
      </>
    );
  }
}

export default Main;
