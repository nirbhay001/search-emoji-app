import React, { Component } from "react";
import "./EmojiRow.css";

class EmojiRow extends Component {
  render() {
    return (
      <div
        className="Emoji-row copy-to-clipboard"
        data-clipboard-text={this.props.symbol}
      >
        <div className="image-row">
          <div className="meme-image">{this.props.symbol}</div>
          <span className="title">{this.props.title}</span>
        </div>
        <span className="Copy-emoji">Click to copy emoji</span>
      </div>
    );
  }
}
export default EmojiRow;
