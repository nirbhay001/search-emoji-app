import React, { Component } from "react";
import EmojiRow from "./EmojisRow/EmojiRow";
import Clipboard from "clipboard";

class Emojis extends Component {
  componentDidMount() {
    this.clipboard = new Clipboard(".copy-to-clipboard");
  }

  componentWillUnmount() {
    this.clipboard.destroy();
  }
  render() {
    return this.props.emojiData.length !== 0 ? (
      this.props.emojiData
        .map((emoji) => {
          return (
            <EmojiRow
              key={emoji.title}
              symbol={emoji.symbol}
              title={emoji.title}
            />
          );
        })
        .slice(0, 25)
    ) : (
      <h1 style={{ textAlign: "center" }}>Meme data not found</h1>
    );
  }
}
export default Emojis;
